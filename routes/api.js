const express = require('express');
const bodyParser = require('body-parser');
var router = express.Router();

const models = require('../model/');

var jsonParser = bodyParser.json()


router.post('/get_total', jsonParser, function (req, res) {
    if (!req.body) return res.sendStatus(400)

    let items = req.body.items;
    getTotal(items).then(total =>{
        
        res.send({ 'total': total });
    } )
        .catch(err => res.sendStatus(400));

    
});

// Esta funcion me la robe de un gist. por que array.foreach(async function()) no funcionaba igual.
const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
    }
}


async function getTotal(items) {
    var total = 0;
    await asyncForEach(items, async (item) => {
        var quantity = item.quantity;
        var product = await models.Item.findOne({
            attributes: ['price', 'code', 'name'],
            where: { code: item.code }
        }).catch((err) => { console.log(err); });

        const totalItem = await getTotalPerItem(product, quantity)

        total = parseFloat(total) + parseFloat(totalItem);
    });
    console.log('done')
 
    return total;
}

async function getTotalPerItem(product, quantity){
    var total = 0;
    var code = product.get('code');
    var price = parseFloat(product.get('price'));
    switch (code) {

        case 'PANTS':
            if (quantity > 1) {
                if (quantity % 2 === 0) {
                    total += price * (quantity / 2);
                } else {
                    quantity = quantity - 1;
                    total += price * (quantity / 2);
                    total += price;
                }
            }
            else {
                total += price * quantity;
            }
            break;
        case 'TSHIRT':
            if (quantity > 2) {
                total += 19.00 * quantity;
            }
            else {
                total += price * quantity;
            }
            break;
        case 'HAT':
            total += price * quantity;
            break;
        default:
            break;
    }

    return total;
}


module.exports = router;
