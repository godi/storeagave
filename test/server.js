const expect = require("chai").expect;
const request = require('superagent');

describe("Api get total test", function () {
    var response;
    describe("Api get total for items", function () {
        const url = "http://localhost:8000/api/get_total";
        it("returns 32.50", function(done) {
            const items = {
                items: [
                    { code: "PANTS", quantity: 1 },
                    { code: "TSHIRT", quantity: 1 },
                    { code: "HAT", quantity: 1 }]
            };

            request
            .post(url)
            .send(items)
            .set('accept', 'json')
            .end((err,res) => {
                
                expect(res.body.total).to.equal(32.5);
                done();
            })
        });

        it("returns 25", function (done) {
            const items = {
                items: [
                    { code: "PANTS", quantity: 2 },
                    { code: "TSHIRT", quantity: 1 }
                    ]
            };

            request
                .post(url)
                .send(items)
                .set('accept', 'json')
                .end((err, res) => {
                    
                    expect(res.body.total).to.equal(25);
                    done();
                });

            
        });

        it("returns 81", function (done) {
            const items = {
                items: [
                    { code: "PANTS", quantity: 1 },
                    { code: "TSHIRT", quantity: 4 }]
            };

            request
                .post(url)
                .send(items)
                .set('accept', 'json')
                .end((err, res) => {
                    
                    expect(res.body.total).to.equal(81);
                    done();
                })

        });

        it("returns 74.50", function (done) {
            const items = {
                items: [
                    { code: "PANTS", quantity: 3 },
                    { code: "TSHIRT", quantity: 3 },
                    { code: "HAT", quantity: 1 }]
            }

            request
                .post(url)
                .send(items)
                .set('accept', 'json')
                .end((err, res) => {
                
                    expect(res.body.total).to.equal(74.5);
                    done();
                })

        });

        it("returns not 74.50", function (done) {
            const items = {
                items: [
                    { code: "PANTS", quantity: 3 },
                    { code: "TSHIRT", quantity: 3 },
                    { code: "HAT", quantity: 4 }]
            }
            //expect(1 + 1).to.equal(52)
            request
                .post(url)
                .send(items)
                .set('accept', 'json')
                .end((err, res) => {
                    expect(res.body.total).to.not.equal(74.5);
                    done();
                })

        });
    });

});