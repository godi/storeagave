'use strict';

const express = require('express');
const bodyParser = require('body-parser');
// Constants
const PORT = 8000;
const HOST = '0.0.0.0';

const api = require('./routes/api');
// App
const app = express();

app.use(bodyParser.json({ type: 'application/*+json' }))
app.use(express.json());
app.use('/api', api);

app.get('/', (req, res) => {
    res.send('Hello-world\n');
});
/**
 * { items : [{code,quantity}]}
 */


app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);



