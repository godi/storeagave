'use strict';
module.exports = (sequelize, DataTypes) => {
    var Item = sequelize.define('Item', {
        code: DataTypes.STRING,
        name: DataTypes.STRING,
        price: DataTypes.DECIMAL
    });



    return Item;
};